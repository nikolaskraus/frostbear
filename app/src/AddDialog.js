import { useState } from 'react'

function AddDialog({hoursbands, setHoursBands, setInDialog, setAdd}) {

    const [showError, setShowError] = useState(false);

    function compareHoursBands(hb1, hb2) {
        if (hb1.min > hb2.min) return -1;
        else if (hb1.min < hb2.min) return 1;
        else return 0;
    }

    function addHourBand() {
        var min = document.getElementById("min").value;
        var max = document.getElementById("max").value;
        var factor = document.getElementById("factor").value;

        if (min >= 0 && max > min && factor > 0 && factor < 1 &&
            hoursbands.findIndex(hourBand => hourBand.min === min && hourBand.max === max
                && hourBand.factor === factor) === -1) {

            var currentHoursBands = hoursbands;
            currentHoursBands.push({min: min, max: max, factor: factor});
            setHoursBands(currentHoursBands.sort(compareHoursBands));

            setShowError(false);
            setAdd(false);
            setInDialog(false);
        }
        else {
            setShowError(true)
        }
    }

    return (<div>
        <div>
        <div>
            <label>Min: </label>
            <input style={{fontSize: "1em"}} type={"number"} id={"min"}/>
        </div>
        <div>
            <label>Max: </label>
            <input style={{fontSize: "1em"}} type={"number"} id={"max"}/>
        </div>
        <div>
            <label>Factor: </label>
            <input style={{fontSize: "1em"}} type={"number"} id={"factor"}/>
        </div>

        <button onClick={addHourBand} style={{margin: "0.5em", fontSize: "1em"}}>Add hour band</button>
        </div>
        {showError ? <label style={{color: "red", margin: "0.5em", fontSize: "1em"}}>
            There was an error while creating the hourband.
            Check if the numbers are correct or if it already exists.</label> : <></>}

    </div>)
}

export default AddDialog