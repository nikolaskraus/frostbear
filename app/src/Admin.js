import HourBandList from "./HourBandList";
import DialogContainer from "./DialogContainer";
import React, {useState} from "react";

function Admin({hoursBands, setHoursBands}) {

const [inDialog, setInDialog] = useState(false);
const [add, setAdd] = useState(false);
const [update, setUpdate] = useState(false);
const [deletion, setDeletion] = useState(false);
const [selected, setSelected] = useState({min: 0, max: 0, factor: 0});

function addHourBand() {
    setInDialog(true);
    setAdd(true);
}

function updateHourBand() {
    setInDialog(true);
    setUpdate(true);
}

function deleteHourBand() {
    setInDialog(true);
    setDeletion(true);
}

function setSelectedValue(e) {
    setSelected(hoursBands[e.target.value]);
}

    return (<div style={{fontSize: "2em"}}>
        { !inDialog ? (
            <div >
            <HourBandList hoursBands={hoursBands} setHourBands={setHoursBands}/>
                <div>
                    <select onChange={setSelectedValue} style={{fontSize: "1em"}}>
                    {hoursBands.map((thisHourBand, index) => {
                        return <option value={index}>Min: {thisHourBand.min} Max: {thisHourBand.max} Factor: {thisHourBand.factor}</option>
                    })}
                    </select>
                </div>
            <button style={{margin: "0.5em", fontSize: "1em"}} onClick={addHourBand}>Add hour band</button>
            <button style={{margin: "0.5em", fontSize: "1em"}} onClick={updateHourBand}>Update hour band</button>
            <button style={{margin: "0.5em", fontSize: "1em"}} onClick={deleteHourBand}>Delete hour band</button>
            </div>
            ) : (<DialogContainer add={add} update={update} deletetion={deletion} hoursBands={hoursBands}
                                  setHoursBands={setHoursBands} setInDialog={setInDialog} setAdd={setAdd} setUpdate={setUpdate} setDeletion={setDeletion} selected={selected}/>)
        }
    </div>)
}

export default Admin;