import './App.css';
import {useEffect, useState} from 'react';
import Admin from './Admin'
import User from './User'
import { BELOW400_FACTOR, BELOW572_FACTOR, BELOW1450_FACTOR, BELOW2204_FACTOR, MAX_FACTOR } from './Constants'

function App() {
   const [hourBands, setHourBands] = useState([BELOW400_FACTOR, BELOW572_FACTOR, BELOW1450_FACTOR, BELOW2204_FACTOR, MAX_FACTOR]);
   const [isAdmin, setIsAdmin] = useState(false);
   const [isLoggedIn, setIsLoggedIn] = useState(false);

   function onClickUser() {
       setIsAdmin(false);
       setIsLoggedIn(true);
   }

   function onClickAdmin() {
       setIsAdmin(true);
       setIsLoggedIn(true);
   }

    function compareHoursBands(hb1, hb2) {
        if (hb1.min > hb2.min) return -1;
        else if (hb1.min < hb2.min) return 1;
        else return 0;
    }

    useEffect(() => {

        setHourBands(hourBands.sort(compareHoursBands))

    },hourBands)

  return (
    <div className="App">
        {!isLoggedIn ? (
            <div className={"ButtonContainer"}>
                <button className={"Button"} onClick={onClickUser}>User</button>
                <button className={"Button"} onClick={onClickAdmin}>Admin</button>
            </div>) : (<div>
            {isAdmin ? <Admin hoursBands={hourBands} setHoursBands={setHourBands} /> : <User hourBands={hourBands}/>}
        </div>)}
    </div>
  );
}

export default App;
