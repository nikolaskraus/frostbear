export const MAX_FACTOR = { min: 2204, max: 8760, factor: 0.9741 };
export const BELOW2204_FACTOR = { min: 1450, max: 2204, factor: 0.91 };
export const BELOW1450_FACTOR = { min: 572, max: 1450, factor: 0.8705 };
export const BELOW572_FACTOR = { min: 400, max: 572, factor: 0.76 };
export const BELOW400_FACTOR = { min: 0, max: 400, factor: 0.6333 };