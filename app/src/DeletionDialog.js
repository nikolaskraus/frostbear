function DeletionDialog({selected, hourBands, setHourbands, setDeletion, setDialog}) {

    function deleteHourBand() {
        var index = hourBands.findIndex(hourBand => hourBand.min === selected.min
            && hourBand.max === selected.max && hourBand.factor === selected.factor)

        var currentHourBands = hourBands;
        currentHourBands.splice(index, 1);
        setHourbands(currentHourBands);

        returnToMenu();
    }

    function returnToMenu() {
        setDeletion(false);
        setDialog(false);
    }

    return (<>
        <div>Are you sure you wanna delete the following hour band?</div>
        <div>Min: {selected.min} Max: {selected.max} Factor: {selected.factor}</div>
        <button onClick={deleteHourBand} style={{fontSize: "1em", margin: "1em", paddingLeft: "1em", paddingRight: "1em"}}>Yes</button>
        <button onClick={returnToMenu} style={{fontSize: "1em", margin: "1em", paddingLeft: "1em", paddingRight: "1em"}}>No</button>
        </>
    )
}

export default DeletionDialog;