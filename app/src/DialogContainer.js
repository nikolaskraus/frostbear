import AddDialog from "./AddDialog";
import UpdateDialog from "./UpdateDialog";
import DeletionDialog from "./DeletionDialog";

function DialogContainer({add, update, deletetion, hoursBands, setHoursBands, setInDialog, setAdd, setUpdate, setDeletion, selected}) {
    if (add)
        return <AddDialog hoursbands={hoursBands} setHoursBands={setHoursBands} setInDialog={setInDialog} setAdd={setAdd}/>
    else if (update)
        return <UpdateDialog hourBands={hoursBands} setHourBands={setHoursBands} selected={selected} setInDialog={setInDialog} setUpdate={setUpdate}/>
    else return <DeletionDialog selected={selected} hourBands={hoursBands} setDeletion={setDeletion} setDialog={setInDialog} setHourbands={setHoursBands} />
}

export default DialogContainer;