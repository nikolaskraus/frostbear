import HourBand from "./HourBand";
import React, {useEffect} from "react";

function HourBandList({hoursBands, setHourBands}) {

    function compareHoursBands(hb1, hb2) {
        if (hb1.min > hb2.min) return -1;
        else if (hb1.min < hb2.min) return 1;
        else return 0;
    }

    useEffect(() => {

        setHourBands(hoursBands.sort(compareHoursBands))

    },hoursBands)

    return (<div>
        {
            hoursBands.map(hourBand => {
                return <HourBand thisHourBand={hourBand}/>
            })
        }


    </div>)
}

export default HourBandList;