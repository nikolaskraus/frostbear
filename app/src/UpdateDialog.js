import {useState} from "react";

function UpdateDialog({hourBands, setHourBands, selected, setInDialog, setUpdate}) {

    const [showError, setShowError] = useState(false);

    function compareHoursBands(hb1, hb2) {
        if (hb1.min > hb2.min) return -1;
        else if (hb1.min < hb2.min) return 1;
        else return 0;
    }

    function updateHourBand() {
        var min = document.getElementById("min").value;
        var max = document.getElementById("max").value;
        var factor = document.getElementById("factor").value;

        if (min >= 0 && max > min &&  factor > 0 && factor < 1 &&
            hourBands.findIndex(hourBand => hourBand.min === min && hourBand.max === max
                && hourBand.factor === factor) === -1) {

            var currentHoursBands = hourBands;
            var index = currentHoursBands.findIndex(hourBand =>
                hourBand.min === selected.min && hourBand.max === selected.max && hourBand.factor === selected.factor
            )
            currentHoursBands[index].max = max;
            currentHoursBands[index].min = min;
            currentHoursBands[index].factor = factor

            setHourBands(currentHoursBands.sort(compareHoursBands));

            setShowError(false);
            setUpdate(false);
            setInDialog(false);
        }
        else {
            setShowError(true)
        }
    }

     return (<div>
        <div>
            <div>
                <label>Min: </label>
                <input style={{fontSize: "1em"}} type={"number"} id={"min"} defaultValue={selected.min}/>
            </div>
            <div>
                <label>Max: </label>
                <input style={{fontSize: "1em"}} type={"number"} id={"max"} defaultValue={selected.max}/>
            </div>
            <div>
                <label>Factor: </label>
                <input style={{fontSize: "1em"}} type={"number"} id={"factor"} defaultValue={selected.factor}/>
            </div>

            <button onClick={updateHourBand} style={{margin: "0.5em", fontSize: "1em"}}>Update hour band</button>
        </div>
        {showError ? <label style={{color: "red", margin: "0.5em", fontSize: "1em"}}>
            There was an error while updating the hourband.
            Check if the numbers are correct or if it already exists.</label> : <></>}

    </div>)
}
export default UpdateDialog;