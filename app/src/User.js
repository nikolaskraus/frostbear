import {useState} from "react";

function User({hourBands}) {

    const [calculatedPension, setCalculatedPension] = useState(0);
    const [isCalculated, setIsCalculated] = useState(false);
    const [factor, setFactor] = useState(0);
    const [showError, setShowError] = useState(false);

    function calculatePension() {
        var salary = document.getElementById("Salary").value;
        var hours = document.getElementById("hours").value;

        if (salary > 0 && hours >= 0 && hours <= 2500) {
            var factor = getFactor(hours)

            setFactor(factor);

            var calc = salary * factor;
            setCalculatedPension(calc);
            setIsCalculated(true);
        }
        else {
            setShowError(true);
        }
    }

    function getFactor(hours) {
        var index = hourBands.findIndex(hourBand => hourBand.min >= hours && hourBand.max > hours)
        return hourBands[index].factor;
    }

    return (
    <div >
        <div>
            <div>
                <label style={{fontSize: "2em"}}>Salary: </label>
                <input style={{fontSize: "2em"}} type={"number"} defaultValue={0} id={"Salary"}/>
            </div>
            <div>
                <label style={{fontSize: "2em"}}>Hours: </label>
                <input style={{fontSize: "2em"}} type={"number"} defaultValue={0} id={"hours"}/>
            </div>
        <button onClick={calculatePension} style={{fontSize: "2em", margin: "1em"}}>Estimate pension</button>
        </div>
        {isCalculated ? (
            <>
            <div>
                <a style={{fontSize: "2em"}}>Your calculated Factor is: {factor}</a>
            </div>
            <div>
                <a style={{fontSize: "2em"}}>Your estimated Pension is: {calculatedPension}</a>
            </div>
            </>
        ): <></>}
        {showError ? <label style={{color: "red", margin: "0.5em", fontSize: "1em"}}>
            There was an error while estimating the pension.
            Check if the salary is above 0 and the hours are between 0 and 2500</label> : <></>}
    </div>    )
}

export default User;